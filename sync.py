import os
from os.path import join, isfile, isdir, splitext
import string
import sys

from sync_pl import *

source = 'E:\OneDrive\Music\- Playlists'
target = 'H:\Resilio\Music\Playlists'

if __name__ == "__main__":
    playlists = [ entry for entry in os.listdir(source) if 
        isfile(join(source, entry)) 
        and ( entry.endswith('.m3u') or entry.endswith('.m3u8')) 
        and "[sync]" in entry ]
    
    for entry in playlists:
        print("Syncing playlist {0}".format(entry))
        SyncPlaylist(target,join(source,entry))
    
    print("Sync complete!")