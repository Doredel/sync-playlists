from os.path import join, isfile, isdir, basename, splitext
import os
from utils import *
from shutil import copyfile

def SyncPlaylist(target,filename):
    """syncs playlist"""
    syncFolder = join(target, splitext(basename(filename))[0].replace("[sync]", "").strip())
    if not isdir(syncFolder): os.makedirs(syncFolder)

    contents = { GetHash(join(syncFolder, entry)):entry for entry in os.listdir(syncFolder) if 
        isfile(join(syncFolder, entry))}

    with open(filename,mode='r',encoding='utf-8-sig') as list:
        update = {
            GetHash(entry[1].strip()):(
                entry[0],
                entry[1].strip(),
                GetTitle(entry[1].strip())+ splitext(basename(entry[1].strip()))[1]) #extract tags
            for entry in enumerate(list.readlines())}
    
    for entry in contents:
        #remove not needed files
        if entry not in update.keys():
            print("Remove {0}".format(basename(contents[entry])))
            os.remove(join(syncFolder,contents[entry]))

    for entry in update:
        entryNum = update[entry][0]
        sourceName = update[entry][1]
        trackName = RepairName(update[entry][2])
        fname = "{0:03d} - {1}".format(entryNum+1,trackName)
        if entry not in contents.keys(): #copy new files
            print("Copy {0}".format(fname))
            copyfile(sourceName,join(syncFolder,fname))
            continue
        if contents[entry] != fname: #rename old files
            print("Rename {0} to {1}".format(basename(contents[entry]),fname))
            os.rename(join(syncFolder,contents[entry]), join(syncFolder,fname))
    
    print("Done!")