from os.path import basename, splitext
import hashlib
from mutagen.easyid3 import EasyID3
from mutagen.flac import FLAC
from mutagen.oggvorbis import OggVorbis
from mutagen.easymp4 import EasyMP4

def GetHash(filename):
    md5_hash = hashlib.md5()
    with open(filename,"rb") as file:
        for chunk in iter(lambda: file.read(4096), b""):
            md5_hash.update(chunk)
    return md5_hash.hexdigest()

def GetTitle(filename):
    fileTitle, fileExt = splitext(basename(filename))
    tags = {}
    if fileExt == '.mp3':
        tags = EasyID3(filename)
    elif fileExt == '.m4a':
        tags = EasyMP4(filename)
    elif fileExt == '.flac':
        tags = FLAC(filename)
    elif fileExt == '.ogg':
        tags = OggVorbis(filename).tags
    else:
        return fileTitle
    return tags.get('title',[fileTitle])[0]

def RepairName(name):
    name = name.replace("\\","-")
    name = name.replace(":","")
    name = name.replace('"',"'")
    name = name.replace("<","")
    name = name.replace(">","")
    name = name.replace("?","")
    name = name.replace("|","")
    name = name.replace("*","")
    return name